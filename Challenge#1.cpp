#include<iostream>
#include<iomanip>

using namespace std;
double PriceCal(double , double);//fucntion prototype
double discount(int , int);
double CentimetreConverter(double &);
double BookSurfaceCal(double , double ,double );
double WeightConverter(double &);
int DiscountCal (int );
double MemberDiscount(double );
void Input_Fuction(double & ,double & , double & , double & , int & );
void Output_Fucntion(double , double ,double );
double AlmostFinalPrice (double [] , int );
double Price4Member (double , double );
void OuTput_For_bill(double [] , int);

int main(){
	int BookAmount;// amount of the books
	double BookPrice[BookAmount];
	double wide , Lenght , Height , Weight , Price; // variable for Book's width , Lenght , Height , Weight , Price
	int Day ; // variable for Day of book stock
	int Discount; // Variable for Discount
	int MemberCheck;
	double DiscountedPrice4Member; // Variable for Member Discounted price
	double Min; // Minimal price for member dissount calculation
	double FinalPrice; // Price that buyer must pay for each book
	
	cout << "How many books will buyer buy ? " ;
	cin >> BookAmount;
	cout << "---------" << endl;
	for( int i = 0 ; i < BookAmount ; i++){
		
		Input_Fuction(Lenght , wide , Height , Weight , Day);					//|	
		
		CentimetreConverter(Lenght);											//|
		CentimetreConverter(wide);												//|	
		WeightConverter(Weight);
		CentimetreConverter(Height);											//|			calculation part
	
		Price = PriceCal(BookSurfaceCal(wide , Lenght , Height) , Weight); 		//|
		Discount = DiscountCal(Day);											//|
		FinalPrice = Price - Discount;											//|
	
		Output_Fucntion(Price , Discount , FinalPrice);		// Price Display			
		
		BookPrice[i] = FinalPrice;
		
		if(i == 0){
			Min = BookPrice[0];
		}
		while(i > 0){
			if(Min > BookPrice[i]){
				Min = BookPrice[i];
			}else { break;
			}
		}
	}
	if(BookAmount >= 3){

		cout << "Member or not?(yes = 1 , no is 0) "; // Member checking and discount for member
		cin >> MemberCheck;
	
		if(MemberCheck == 1){
			cout << "You got 20% off for cheapest book!" << endl;
			for(int i = 0 ; i < BookAmount ; i++ ){
				if(BookPrice[i] == Min){
				DiscountedPrice4Member = MemberDiscount(BookPrice[i]);
				}		
			}
			cout << setw(15) <<" Your discount for cheapest book is :  " << DiscountedPrice4Member << endl;
			cout << setw(15) << "-----------------------------" << endl;
			OuTput_For_bill(BookPrice , BookAmount);
			cout << setw(15) << "Total price: " << Price4Member (AlmostFinalPrice (BookPrice , BookAmount) , DiscountedPrice4Member) <<endl;
			cout << setw(15) <<"------------------------------" << endl;
			cout << setw(15) << "Thank you for coming ! :)";	
		}else{
			cout << setw(15) <<"------------------------------" << endl;
			OuTput_For_bill(BookPrice , BookAmount);
			cout << setw(15) << "Total price : " << AlmostFinalPrice (BookPrice , BookAmount) << endl;
			cout << setw(15) <<"------------------------------" << endl;
			cout << setw(15) << "Thank you for coming ! :)";
		}
	}else{
			cout << setw(15) <<"------------------------------" << endl;
			OuTput_For_bill(BookPrice , BookAmount);
			cout << setw(15) << "Total price : " << AlmostFinalPrice (BookPrice , BookAmount) << endl;
			cout << setw(15) <<"------------------------------" << endl;
			cout << setw(15) << "Thank you for coming ! :)";
	}
	return 0;
}


double CentimetreConverter(double &Lenght_inch){ //conver anynumber in inch into Centimetre
	return Lenght_inch = Lenght_inch * 2.54;
}

double WeightConverter(double &Weight_gramm){//convert Wieght gramm into pound
	return Weight_gramm = Weight_gramm *0.00220462262;
}

double BookSurfaceCal(double Width , double Lenght , double Height){ //surface calculation
	return  ((Width * Lenght) + (Lenght * Height) + (Width * Height))*2;
}

double PriceCal (double BookSurface , double BookWeight ){ //price calculation
	return BookSurface*BookWeight*10;
}

int DiscountCal (int Day){//Discount cal culation
	if(Day > 30){
		return 20 + (Day - 30) * 2;
	}else if(Day > 10){
		return (Day - 10) * 1;
	}else return 0;
}

double MemberDiscount(double Min){
		return Min * 0.2;
}

double AlmostFinalPrice (double Book[] , int bOOkAMount){ // for sum up total price before discount if customer is member
	double ALMOST_FINALPRICE = 0;
	for(int i = 0 ; i < bOOkAMount ; i++){
		ALMOST_FINALPRICE = ALMOST_FINALPRICE + Book[i];
	}
	return ALMOST_FINALPRICE;
}

double Price4Member (double Price , double Discount){
	return Price - Discount;
}

void Input_Fuction(double &Lenght , double &Width , double &Height , double &Weight , int &Day){ //for inputing
	cout << "Book's Lenght : " ;
	cin >> Lenght;
	cout << "Book's Width : " ;
	cin >> Width;
	cout << "Book's Height : ";
	cin >> Height;
	cout<< "Book's Weight : ";
	cin >> Weight;
	cout << "Days in Stock : ";
	cin >> Day;
}


void Output_Fucntion(double Price , double Discount , double FinalPrice){ //bill display
	cout << setw(5) << "Price" << setw(10) << "    Discount" << endl;
	cout << setw(5) << Price << setw(7) << Discount <<endl;
	cout << "----------" << endl;
}

void OuTput_For_bill(double Price[] , int BookAMOUNT ){
	cout << setw(15) << "Price" << endl;
	for(int i = 0 ; i < BookAMOUNT ; i++){
		cout << "Book " << i+1 << setw(9) << Price[i] << endl; 
	}
	cout << setw(15) << "---------------" << endl;
}
